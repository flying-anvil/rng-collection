<?php

declare(strict_types=1);

namespace FlyingAnvil\RngCollection\Test;

use FlyingAnvil\RngCollection\SM64;
use PHPUnit\Framework\TestCase;

/**
 * @covers \FlyingAnvil\RngCollection\SM64
 */
class SM64Test extends TestCase
{
    public function testCanGenerateWithEvenS0(): void
    {
        $generator = new SM64();
        $result    = $generator->generate();

        self::assertLessThan(0x10000, $result);
        self::assertGreaterThan(-1, $result);
    }

    public function testCanGenerateWithOddS0(): void
    {
        $generator = new SM64(13);
        $result    = $generator->generate();

        self::assertLessThan(0x10000, $result);
        self::assertGreaterThan(-1, $result);
    }

    public function testCycleIsResettetPrematurely(): void
    {
        $generator = new SM64(21674);
        $result    = $generator->generate();

        self::assertEquals(0, $result);
    }

    public function testFailsafeIsSafe(): void
    {
        $generator = new SM64(58704);

        $result = $generator->generate();
        self::assertEquals(22026, $result);

        $result = $generator->generate();
        self::assertEquals(57460, $result);
    }

    /**
     * @coversNothing
     */
    public function testGeneratedSequenceIsCorrect(): void
    {
        $generator    = new SM64();
        $result       = -1;
        $resultString = '';

        for ($i = 0; $i < 65114; $i++) {
            $result = $generator->generate();

            $resultString .= $result . PHP_EOL;
        }

        self::assertEquals(0, $result);
        self::assertStringEqualsFile(__DIR__ . '/_files/SM64-expected.txt', $resultString);
    }
}
