<?php

declare(strict_types=1);

namespace FlyingAnvil\RngCollection;

class SM64 implements RandomNumberGeneratorInterface
{
    /** @var int */
    private $current;

    public function __construct(int $current = 0)
    {
        $this->current = $current;
    }

    public function generate(): int
    {
        $result = $this->current;

        // Unused failsafe
        if ($result === 0x560A) {
            $result = 0;
        }

        // % 0x10000 = "casting" into short;
        $s0 = (($result << 8) % 0x10000);
        $s0 ^= $result;

        // Flip bytes
        $result = ((($s0 & 0xFF) << 8) % 0x10000) | (($s0 & 0xFF00) >> 8);

        $s0 = (($s0 % 0x100) << 1) ^ $result;
        $s1 = ($s0 >> 1) ^ 0xFF80;

        if (($s0 & 1) === 0) {
            if ($s1 === 0xAA55) {
                $this->current = 0;
                return 0;
            }

            $result = $s1 ^ 0x1FF4;
            $this->current = $result;
            return $result;
        }

        $result = $s1 ^ 0x8180;
        $this->current = $result;
        return $result;
    }

    public function generateRange(int $min, int $max): int
    {
        $diff = ($max - $min);
        if ($diff > 0xFFFF) {
            // TODO: throw better exception
            throw new \Exception('Cannot generate value, difference between min and max must be less than 65536');
        }

        $result = $this->generate();

        if ($result > $diff) {
            $result = (($result + $max) % ($diff + 1));
        }

        return $result + $min;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }
}
