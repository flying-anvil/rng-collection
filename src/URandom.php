<?php

declare(strict_types=1);

namespace FlyingAnvil\RngCollection;

class URandom implements RandomNumberGeneratorInterface
{
    /** @var int */
    private $current;

    /** @var int */
    private $resolution;

    /** @var resource */
    private static $handle;

    public function __construct(int $resolution = 2)
    {
        if (!self::$handle) {
            if (!file_exists('/dev/urandom')) {
                // TODO: throw better exception
                throw new \Exception('Cannot open "/dev/urandom". Maybe your platform is not supported');
            }

            self::$handle = fopen('/dev/urandom', 'rb');
        }

        $this->resolution = $resolution;
    }

    public function generate(): int
    {
        return $this->generateRange(0, 1 << ($this->resolution * 8));
    }

    public function generateRange(int $min, int $max): int
    {
        $diff   = $max - $min;
        $bytes  = (int)ceil(($diff + 1) / 256);
        $read   = fread(self::$handle, $bytes);
        $result = 0;

        for ($i = 0; $i < $bytes; $i++) {
            $result += ord($read[$i]);
        }

        if ($result > $diff) {
            // TODO: $max has double chances
            $result = (($result + $max) % ($diff + 1));
        }

        return $result + $min;
    }

    public function getCurrent(): int
    {
        return $this->current;
    }
}
