<?php

declare(strict_types=1);

namespace FlyingAnvil\RngCollection;

interface RandomNumberGeneratorInterface
{
    public function generate(): int;

    public function generateRange(int $min, int $max): int;

    public function getCurrent(): int;
}
