<?php

use FlyingAnvil\RngCollection\SM64;
use FlyingAnvil\RngCollection\URandom;

require_once __DIR__ . '/vendor/autoload.php';

$sm64    = new SM64();
$uRandom = new URandom();

$rng = $sm64;

for ($i = 0; $i < 1000; $i++) {
    $result = $rng->generate(0,256);
    echo 'dec: ' . str_pad($result, 0, '0', STR_PAD_LEFT), PHP_EOL;
//    echo 'hex: ' . dechex($result), '   (2917)', PHP_EOL;
}

//$resultMap = [];
//
//for ($i = 0; $i < 100000; $i++) {
//    $resultMap[$uRandom->generateRange(0, 300)]++;
//}
//
//asort($resultMap);
//
//var_dump($resultMap);
